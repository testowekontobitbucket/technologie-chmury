<?php
/**
 * Created by PhpStorm.
 * User: Mateusz
 * Date: 13.01.2019
 * Time: 03:16
 */

namespace App\Controller;


use App\Form\HomeForm;
use App\Service\TextToSpeechService;
use App\Service\TranslatorService;
use App\Service\VisionService;
use Google\ApiCore\ApiException;
use Google\ApiCore\ValidationException;
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SsmlVoiceGender;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;
use Google\Cloud\Translate\TranslateClient;
use Google\Cloud\Vision\Annotation;
use Google\Cloud\Vision\VisionClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/index")
     * @param Request $request
     * @return Response
     * @throws ApiException
     * @throws ValidationException
     */
    public function indexAction(Request $request) {
        $form = $this->createForm(HomeForm::class);
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $url = $form->get('url')->getData();

            $foundTexts = (new VisionService())->findTextsOnImage($url);
            $translations = (new TranslatorService())->translateTexts($foundTexts);
            $filename = (new TextToSpeechService())->generateMp3FromTexts($translations);

            return $this->render('player.html.twig', [
                'filename' => $filename,
                'url' => $url,
                'foundTexts' => $foundTexts,
                'translations' => $translations
            ]);
        }
        else {
            return $this->render('form.html.twig', [
                'form' => $form->createView()
            ]);
        }

    }
}