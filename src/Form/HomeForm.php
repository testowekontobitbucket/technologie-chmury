<?php
/**
 * Created by PhpStorm.
 * User: Mateusz
 * Date: 13.01.2019
 * Time: 04:29
 */

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class HomeForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('url', TextType::class, [
            'data' => "https://i.pinimg.com/originals/89/09/a9/8909a932f8b931215889740a0610cb8b.jpg"
        ]);
        $builder->add('submit', SubmitType::class);
    }
}