<?php
/**
 * Created by PhpStorm.
 * User: Mateusz
 * Date: 13.01.2019
 * Time: 16:42
 */

namespace App\Service;


use Google\ApiCore\ApiException;
use Google\ApiCore\ValidationException;
use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SsmlVoiceGender;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;

class TextToSpeechService
{
    public function generateMp3FromTexts($translations) {
        $textToSpeech = null;
        try {
            $textToSpeech = new TextToSpeechClient();
        } catch (ValidationException $e) {
        }
        $inputText = (new SynthesisInput())->setText($translations[0]);
        $voice = (new VoiceSelectionParams())
            ->setLanguageCode('en-US')
            ->setSsmlGender(SsmlVoiceGender::FEMALE);


        $audioConfig = (new AudioConfig())
            ->setAudioEncoding(AudioEncoding::MP3);

        $synthesized = null;
        try {
            $synthesized = $textToSpeech->synthesizeSpeech($inputText, $voice, $audioConfig);
        } catch (ApiException $e) {
        }
        $audioContent = $synthesized->getAudioContent();

        $filename = 'output.mp3';
        file_put_contents($filename, $audioContent);

        $textToSpeech->close();

        return $filename;
    }
}