<?php
/**
 * Created by PhpStorm.
 * User: Mateusz
 * Date: 13.01.2019
 * Time: 16:40
 */

namespace App\Service;


use Google\Cloud\Translate\TranslateClient;

class TranslatorService
{
    public function translateTexts($foundTexts) {
        $translatorApi = new TranslateClient();

        $translations = [];

        foreach ($foundTexts as $foundText) {
            $text = $foundText->info();
            $translation = $translatorApi->translate($text["description"],
                ["source" => "pl", "target" => "en"]
            );
            $translations[] = $translation["text"];
        }

        return $translations;
    }
}