<?php
/**
 * Created by PhpStorm.
 * User: Mateusz
 * Date: 13.01.2019
 * Time: 16:38
 */

namespace App\Service;


use Google\Cloud\Vision\Annotation;
use Google\Cloud\Vision\VisionClient;

class VisionService
{
    public function findTextsOnImage($url) {
        $visionApi = new VisionClient();
        $image = $visionApi->image(file_get_contents($url), ['text']);
        /** @var Annotation $annotation */
        $annotation = $visionApi->annotate($image);
        return $annotation->text();
    }
}